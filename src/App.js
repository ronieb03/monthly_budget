import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App" class='container-fluid'>
      <div class='row'>
      <div class="col-md-5 my-3">
      <form id="budget-form" class="budget-form">
        <h5 class="text-capitalize">please enter your savings description</h5>
          <input type="number" min='0' placeholder='0' class="form-control" id="budget-input" /><br/>
          <button type="submit" class="btn btn-success" id="budget-submit">Calculate</button>
      </form>
      </div>

      <br/>

      <div class="col-md-7">
      <div class="row my-3">
       <div class="col-4 text-center mb-2">
        <h6 class="text-uppercase info-title">budget</h6>
        <h4 class="text-uppercase mt-2 budget" id="budget"><span>$ </span><span id="budget-amount">0</span></h4>
       </div>
       <div class="col-4 text-center">
        <h6 class="text-uppercase info-title">expenses</h6>
        <h4 class="text-uppercase mt-2 expense" id="expense"><span>$ </span><span id="expense-amount">0</span></h4>
       </div>
       <div class="col-4 text-center">
        <h6 class="text-uppercase info-title">balance</h6>
        <h4 class="text-uppercase mt-2 balance" id="balance"> <span>$ </span><span id="balance-amount">0</span></h4>
       </div>
      </div>
     </div>
     </div>
     <div class='row'>
     <div class="col-md-5 my-3">
      <form id="expense-form" class="expense-form">
       <h5 class="text-capitalize">please enter your expense</h5>
       <div class="form-group">
        <input type="text" min='0' placeholder='0' class="form-control expense-input" id="expense-input" />
       </div>
       <h5 class="text-capitalize">please enter expense amount</h5>
       <div class="form-group">
        <input type="number" min='0' placeholder='0' class="form-control expense-input" id="amount-input" />
       </div>
       <button type="submit" class="btn btn-danger expense-submit" id="expense-submit">Add Expense</button>
      </form>
     </div>
     <div class="col-md-7 my-3">
      <div class="expense-list" id="expense-list">

       <div class="expense-list__info d-flex justify-content-between text-capitalize">
        <h5 class="list-item">expense description</h5>
        <h5 class="list-item">expense value</h5>
        <h5 class="list-item"></h5>
       </div>

      </div>
     </div>
     </div>
    </div>
  );
}

export default App;
